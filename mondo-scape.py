import requests
from selenium import webdriver 
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC 
from selenium.common.exceptions import TimeoutException
import re
import csv
import time 


from bs4 import BeautifulSoup




def editionSize(product_description):

    if (re.search('Edition of (\d*).', product_description.text)):
        edition_size = re.search('Edition of (\d*).', product_description.text)
        edition_size = edition_size.group(1).strip()
    elif (re.search('Edition Of: (\d*).', product_description.text)):
        edition_size = re.search('Edition Of:(\d*).', product_description.text)
        edition_size = edition_size.group(1).strip()
    elif (re.search('Edition Size: (\d*).', product_description.text)) :
        edition_size = re.search('Edition Size:(\d*).', product_description.text)
        edition_size = edition_size.group(1).strip()
    else: 
        edition_size = "null"


    return edition_size

def getMeasurements(product_description):

    if(re.search('Height:(.*)', product_description.text) and re.search('Height:(.*)', product_description.text)) :
        height = re.search('Height:(.*)', product_description.text)
        height = height.group(1).strip().replace('"',"")
        width = re.search('Width:(.*)', product_description.text)
        width = width.group(1).strip().replace('"',"")
    else: 
        measurements = re.findall("\d+(?: \d*)?(?:\"|”)", product_description.text)
        if measurements :
            width = measurements[0].replace('"',"").replace("”","")
            height = measurements[1].replace('"',"").replace("”","")
        else: 
            height = 'null'
            width = 'null'
            
    return {'width': width, 'height': height}


mondo_base_url = "https://mondotees.com"
#path to chrome driver
driver = webdriver.Chrome("")

driver.get("https://mondotees.com/collections/posters?_=pf&pf_st_stock_status=true&pf_st_stock_status=false")
# driver.get("https://mondotees.com/collections/posters?_=pf&pf_st_stock_status=true&pf_st_stock_status=false&page=26")
# Wait 20 seconds for page to load
timeout = 5
col_headers = ['title', 'artist', 'vendor', 'picture_url', 'edition_size', 'technique', 'manufacturer', 'original_price', 'release_date', 'width', 'height', 'status']
with open('mondo.csv', mode='w') as mondo_file:
    mondo_writer = csv.writer(mondo_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    mondo_writer.writerow(col_headers)
    try:

        WebDriverWait(driver, timeout).until(EC.visibility_of_element_located((By.CLASS_NAME, "pagination-custom")))
        pagination = driver.find_element_by_class_name('pagination-custom')
        soup = BeautifulSoup(pagination.get_attribute('innerHTML'), 'html.parser')
        number_of_pages = soup.findAll('li')[4].text
        for x in range (1,int(number_of_pages)):
            time.sleep(8)
            print("page "+str(x))

            collectionPage = BeautifulSoup(driver.page_source, 'html.parser')
            printContainers = collectionPage.findAll('div',{'id','product-grid-item'})
            for printContainer in printContainers:

                # print_page = requests.get("https://mondotees.com/collections/posters/products/werewolves-on-wheels-variant-phantom-city-creative-poster?variant=830366159")
                print_page = requests.get(mondo_base_url+printContainer.a.attrs['href'])
                soup = BeautifulSoup(print_page.text, 'html.parser')

                title = soup.find('h2',{'class', 'product-title'}).text.strip()
                print(title)

                artist = soup.find('span',{'class', 'product-vendor'}).find('a').text.strip()
                image_url = soup.find('div', {'class', 'product-image-container'}).find('img').attrs['src']
                original_price = soup.find('span', {'class', 'product-price'}).text.split('.')[0].replace("$","").strip()

                detail_specs = soup.findAll('span', {'class', 'product-spec-value'})
                product_description = soup.find('div', {'class', 'product-description'})

                if len(detail_specs) == 0 :
                    
                    edition_size = editionSize(product_description)
                    
                    measurements = getMeasurements(product_description)
                    height = measurements['height']
                    width = measurements['width']

                elif len(detail_specs) == 2 :
                    if (re.search('timed-edition', product_description.text)):
                        edition_size = "Timed Edition"
                    else :
                        edition_size = "null"
                    product_description = soup.find('div', {'class', 'product-description'})
                    edition_size = "Timed Edition"
                    height = detail_specs[0].text.replace('"',"")
                    width = detail_specs[1].text.replace('"',"")
                elif len(detail_specs) == 4 : 
                    edition_size = detail_specs[1].text.replace('"',"")
                    height = detail_specs[2].text.replace('"',"")
                    width = detail_specs[3].text.replace('"',"")
                else :
                    if (re.search('timed-edition', product_description.text)):
                        edition_size = "Timed Edition"
                    else :
                        edition_size = detail_specs[0].text
                    height = detail_specs[1].text.replace('"',"")
                    width = detail_specs[2].text.replace('"',"")


                # print(title + "  ===> " + edition_size)

                product_details = soup.find('div', {'class', 'product-description'})
                # manufacturer = re.search('Printed by(.*).', product_details.text)
                manufacturer = re.search('Printed by \s*(.*?)\s*\.', product_details.text)

                if manufacturer :
                    manufacturer = manufacturer.group(1).strip()
                else: 
                    manufacturer = ""
                mondo_writer.writerow([title, artist, 'Mondo', 'https:'+image_url, edition_size,'technique', manufacturer,original_price, 'null', width, height, 'Official', x])

            next_page = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CLASS_NAME, "icon-arrow-right")))
            next_page.click()






        driver.quit()
      

    except TimeoutException:
        print("Timed out waiting for page to load")
        driver.quit() 

