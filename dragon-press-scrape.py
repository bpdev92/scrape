import requests
from selenium import webdriver 
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC 
from selenium.common.exceptions import TimeoutException
import re
import csv
import time 


from bs4 import BeautifulSoup




def editionSize(product_description):

    if (re.search('Edition of (\d*).', product_description.text)):
        edition_size = re.search('Edition of (\d*).', product_description.text)
        edition_size = edition_size.group(1).strip()
    elif (re.search('Edition Of: (\d*).', product_description.text)):
        edition_size = re.search('Edition Of:(\d*).', product_description.text)
        edition_size = edition_size.group(1).strip()
    elif (re.search('Edition Size: (\d*).', product_description.text)) :
        edition_size = re.search('Edition Size:(\d*).', product_description.text)
        edition_size = edition_size.group(1).strip()
    else: 
        edition_size = "null"


    return edition_size

def getMeasurements(product_description):

    if(re.search('Height:(.*)', product_description.text) and re.search('Height:(.*)', product_description.text)) :
        height = re.search('Height:(.*)', product_description.text)
        height = height.group(1).strip().replace('"',"")
        width = re.search('Width:(.*)', product_description.text)
        width = width.group(1).strip().replace('"',"")
    else: 
        measurements = re.findall("\d+(?: \d*)?(?:\"|”)", product_description.text)
        if measurements :
            width = measurements[0].replace('"',"").replace("”","")
            height = measurements[1].replace('"',"").replace("”","")
        else: 
            height = 'null'
            width = 'null'
            
    return {'width': width, 'height': height}


mondo_base_url = "https://mondotees.com"
#path to chrome driver
driver = webdriver.Chrome("")

driver.get("https://www.blackdragonpress.co.uk/collections/movie-posters")
# Wait 20 seconds for page to load
timeout = 5
col_headers = ['title', 'artist', 'vendor', 'picture_url', 'edition_size', 'technique', 'manufacturer', 'original_price', 'release_date', 'width', 'height', 'status']
with open('blackdragonpress.csv', mode='w') as printFile:
    print_writer = csv.writer(printFile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    print_writer.writerow(col_headers)
    try:

      collectionPage = BeautifulSoup(driver.page_source, 'html.parser')
      printContainers = collectionPage.findAll('div',{'id','product-grid-item'})
      for printContainer in printContainers:

          print_page = requests.get(mondo_base_url+printContainer.a.attrs['href'])
          soup = BeautifulSoup(print_page.text, 'html.parser')


          print_writer.writerow([title, artist, 'Mondo', 'https:'+image_url, edition_size,'technique', manufacturer,original_price, 'null', width, height, 'Official', x])


          driver.quit()
      

    except TimeoutException:
        print("Timed out waiting for page to load")
        driver.quit() 

